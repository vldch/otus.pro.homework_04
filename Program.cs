﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace homework_04
{
    public class Program
    {
        private const int CircleCount = 1_000_000;

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            OneObject();

            ListObjects();
        }

        /// <summary>
        /// Тестирование одного объекта
        /// </summary>
        /// <param OneObject="args"></param>
        private static void OneObject()
        {
            var fooObject = new Foo().Get();

            Console.WriteLine("Тестирование одного элемента");

            var startTime = System.Diagnostics.Stopwatch.StartNew();

            var s = "";

            for (var i = 0; i < CircleCount; i++)
            {
                s = Serializer<Foo>.Serialize(fooObject);
            }

            startTime.Stop();

            Console.WriteLine($"CSV Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            for (var i = 0; i < CircleCount; i++)
            {
                _ = Deserializer<Foo>.DeserializeOne(s);
            }

            startTime.Stop();

            Console.WriteLine($"CSV Deserialized time: {Format.ResultTime(startTime.Elapsed)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            for (var i = 0; i < CircleCount; i++)
            {
                s = JsonConvert.SerializeObject(fooObject);
            }

            startTime.Stop();

            Console.WriteLine($"JsonConvert Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            for (var i = 0; i < CircleCount; i++)
            {
                _ = JsonConvert.DeserializeObject<Foo>(s);
            }

            startTime.Stop();

            Console.WriteLine($"JsonConvert Deserialized time: {Format.ResultTime(startTime.Elapsed)}");
        }

        /// <summary>
        /// Тестирование списка объектов
        /// </summary>
        /// <param ListObjects="args"></param>
        private static void ListObjects()
        {
            Console.WriteLine("Тестирование списка");

            var fooList = new List<Foo>();

            for (var i = 0; i < CircleCount; i++)
            {
                fooList.Add(new Foo().Get());
            }

            var startTime = System.Diagnostics.Stopwatch.StartNew();

            var s = Serializer<Foo>.Serialize(fooList);

            startTime.Stop();

            var serializationTime = startTime.Elapsed;


            startTime = System.Diagnostics.Stopwatch.StartNew();

            Console.WriteLine(s);

            startTime.Stop();

            Console.WriteLine($"Время на вывод в консоль: {Format.ResultTime(startTime.Elapsed)}");

            Console.WriteLine($"CSV Serialized time: {Format.ResultTime(serializationTime)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            _ = Deserializer<Foo>.Deserialize(s);

            startTime.Stop();

            Console.WriteLine($"CSV Deserialized time: {Format.ResultTime(startTime.Elapsed)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            var JSONs = JsonConvert.SerializeObject(fooList);

            startTime.Stop();

            Console.WriteLine($"JsonConvert Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            startTime = System.Diagnostics.Stopwatch.StartNew();

            _ = JsonConvert.DeserializeObject<List<Foo>>(JSONs);

            startTime.Stop();

            Console.WriteLine($"JsonConvert Deserialized time: {Format.ResultTime(startTime.Elapsed)}");
        }
    }

}

