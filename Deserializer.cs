﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace homework_04
{
    public static class Deserializer<T>
    {
        public static List<T> Deserialize(string str)
        {
            var res = new List<T>();

            var list = SplitString(str);

            var names = list[0].Split(';');

            var myType = typeof(T);

            for (var i = 1; i < list.Length - 1; i++)
            {
                var objectValues = list[i].Split(';');

                var instance = Activator.CreateInstance(myType);

                for (var j = 0; j < names.Length; j++)
                {
                    var field = myType.GetField(names[j]);

                    field.SetValue(instance, ConvertToType(field, objectValues[j]));
                }
                res.Add((T)instance);
            }

            return res;
        }

        public static T DeserializeOne(string str)
        {
            var list = Deserialize(str);

            return list[0];
        }

        private static string[] SplitString(string str)
        {
            return str.Split("\r\n");
        }

        private static dynamic ConvertToType(FieldInfo field, string value)
        {
            if (field.FieldType == typeof(int))
            {
                return int.Parse(value);
            }

            if (field.FieldType == typeof(string))
            {
                return value;
            }

            return value;
        }
    }

}
